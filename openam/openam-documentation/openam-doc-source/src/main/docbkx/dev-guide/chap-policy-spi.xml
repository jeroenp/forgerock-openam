<?xml version="1.0" encoding="UTF-8"?>
<!--
  ! CCPL HEADER START
  !
  ! This work is licensed under the Creative Commons
  ! Attribution-NonCommercial-NoDerivs 3.0 Unported License.
  ! To view a copy of this license, visit
  ! http://creativecommons.org/licenses/by-nc-nd/3.0/
  ! or send a letter to Creative Commons, 444 Castro Street,
  ! Suite 900, Mountain View, California, 94041, USA.
  !
  ! You can also obtain a copy of the license at
  ! src/main/resources/legal-notices/CC-BY-NC-ND.txt.
  ! See the License for the specific language governing permissions
  ! and limitations under the License.
  !
  ! If applicable, add the following below this CCPL HEADER, with the fields
  ! enclosed by brackets "[]" replaced with your own identifying information:
  !      Portions Copyright [yyyy] [name of copyright owner]
  !
  ! CCPL HEADER END
  !
  !      Copyright 2011-2014 ForgeRock AS
  !    
-->
<chapter xml:id='chap-policy-spi'
 xmlns='http://docbook.org/ns/docbook'
 version='5.0' xml:lang='en'
 xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
 xsi:schemaLocation='http://docbook.org/ns/docbook
                     http://docbook.org/xml/5.0/xsd/docbook.xsd'
 xmlns:xlink='http://www.w3.org/1999/xlink'>
 <title>Customizing Policy Evaluation</title>
 <indexterm>
  <primary>Policy</primary>
  <secondary>Plugins</secondary>
 </indexterm>
 <para>OpenAM policies let you restrict access to resources based both on
 identity and group membership, and also on a range of conditions including
 session age, authentication chain or module used, authentication level, realm,
 session properties, IP address and DNS name, user profile content, resource
 environment, date, day, time of day, and time zone. Yet, some deployments
 require further distinctions for policy evaluation. This chapter explains how
 to customize policy evaluation for deployments with particular requirements
 not met by built-in OpenAM functionality.</para>
 
 <!-- Inspired by the sample plugin provided with OpenAM and Sam's wiki work at
      https://wikis.forgerock.org/confluence/display/openam/Install+Custom+Policy+Plug-in -->
 
 <para>
  This chapter shows how to build sample plugins,
  and how to configure OpenAM to use one of the plugins.
 </para>

 <section xml:id="about-sample-policy-plugins">
  <title>About the Sample Plugins</title>
  
  <para>
   The OpenAM policy framework lets you build plugins
   to extend subjects, conditions, and response providers for policies,
   and also extend referrals for policy delegation.
   The <link xlink:href="https://github.com/markcraig/openam-policy-eval-sample"
   xlink:show="new">sample policy evaluation plugin source</link>
   is available online.
   Get a local clone so that you can read the source and try the sample on your system.
   In the sources you find the following files.
  </para>
  
  <variablelist>
   <varlistentry>
    <term><filename>pom.xml</filename></term>
    <listitem>
     <para>
      Apache Maven project file for the module
     </para>

     <para>
      This file specifies how to build the sample policy evaluation plugin,
      and also specifies its dependencies on OpenAM components.
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><filename>src/main/java/org/forgerock/openam/examples/SampleCondition.java</filename></term>
    <listitem>
     <para>
      Extends the <literal>Condition</literal> interface,
      and shows an implementation of a condition
      to base the policy decision on the length of the user name
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><filename>src/main/java/org/forgerock/openam/examples/SampleReferral.java</filename></term>
    <listitem>
     <para>
      Extends the <literal>Referral</literal> interface,
      and shows an implementation of a policy referral for delegation
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><filename>src/main/java/org/forgerock/openam/examples/SampleResponseProvider.java</filename></term>
    <listitem>
     <para>
      Extends the <literal>ResponseProvider</literal> interface,
      and shows an implementation of a response provider
      to send an attribute from the user profile with the response
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><filename>src/main/java/org/forgerock/openam/examples/SampleSubject.java</filename></term>
    <listitem>
     <para>
      Extends the <literal>Subject</literal> interface,
      and shows an implementation
      that defines the users to whom the policy applies,
      in this case all authenticated users
     </para>
    </listitem>
   </varlistentry>
  </variablelist>
  
  <para>
   Build the module using Apache Maven.
  </para>

  <screen>
$ <userinput>cd /path/to/openam-policy-eval-sample</userinput>
$ <userinput>mvn install</userinput>
<computeroutput>[INFO] Scanning for projects...
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] Building openam-policy-eval-sample 1.0.0-SNAPSHOT
[INFO] ------------------------------------------------------------------------

...

[INFO] --- maven-jar-plugin:2.3.1:jar (default-jar) @ openam-policy-eval-sample
[INFO] Building jar: .../target/openam-policy-eval-sample-1.0.0-SNAPSHOT.jar
[INFO]

...

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.827s
[INFO] Finished at: Mon Nov 25 19:07:15 CET 2013
[INFO] Final Memory: 21M/228M
[INFO] ------------------------------------------------------------------------</computeroutput>
  </screen>

  <para>
   Copy the .jar to the <filename>WEB-INF/lib</filename> directory
   where you deployed OpenAM.
  </para>

  <screen>
$ <userinput>cp target/*.jar /path/to/tomcat/webapps/openam/WEB-INF/lib/</userinput>
  </screen>

  <para>
   Restart OpenAM or the container in which it runs.
  </para>
 </section>
 
 <section xml:id="configure-sample-policy-plugin">
  <title>Configuring A Sample Policy Plugin</title>
  
  <para>
   This section shows how to configure the sample custom policy condition.
   Configuration involves defining the strings that describe the policy condition,
   and plugging the policy condition into the <literal>iPlanetAMPolicyService</literal>,
   and then restarting OpenAM to be able to add the condition to your policies.</para>
  
  <para>
   You need to update two files under your OpenAM deployment
   where the strings describing your plugin are included as properties values.
  </para>

  <para>
   Extract <filename>amPolicy.properties</filename>,
   <filename>amPolicyConfig.properties</filename>,
   and if necessary the localized versions of this file
   from <filename>${coreLibrary}</filename>
   to <filename>WEB-INF/classes/</filename> where OpenAM is deployed.
   For example, if OpenAM is deployed under
   <filename>/path/to/tomcat/webapps/openam</filename>,
   then you could run the following commands.
  </para>

  <screen>
$ <userinput>cd /path/to/tomcat/webapps/openam/WEB-INF/classes/</userinput>
$ <userinput>jar -xvf ../lib/${coreLibrary} amPolicy.properties \
 amPolicyConfig.properties</userinput>
 <computeroutput>inflated: amPolicyConfig.properties
 inflated: amPolicy.properties</computeroutput>
  </screen>

  <para>
   In <filename>amPolicy.properties</filename> add this property:
  </para>
  
  <literallayout class="monospaced">samplecondition-policy-name=Sample Condition</literallayout>
 
  <para>
   In <filename>amPolicyConfig.properties</filename> add this property:</para>
  
  <literallayout class="monospaced">x100=Sample Condition</literallayout>
  
  <para>Add the schema that describes your plugin to OpenAM.</para>
  
  <screen>
$ <userinput>ssoadm \
 add-plugin-schema \
 --adminid amadmin \
 --password-file /tmp/pwd.txt \
 --servicename iPlanetAMPolicyService \
 --interfacename Condition \
 --pluginname SampleCondition \
 --i18nname amPolicy \
 --i18nkey samplecondition-policy-name \
 --classname org.forgerock.openam.examples.SampleCondition</userinput>

<computeroutput>Plug-in schema, Condition was added to service, iPlanetAMPolicyService.</computeroutput>
  </screen>
  
  <para>
   Set the choice values of the schema to include your plugin
   with other policy conditions in the policy service.
  </para>
  
  <screen>
$ <userinput>ssoadm \
 set-attr-choicevals \
 --adminid amadmin \
 --password-file /tmp/pwd.txt \
 --servicename iPlanetAMPolicyConfigService \
 --schematype Organization \
 --attributename iplanet-am-policy-selected-conditions \
 --add \
 --choicevalues "x100=SampleCondition" </userinput>

<computeroutput>Choice Values were set.</computeroutput>
  </screen>
  
  <para>
   Set the plugin policy condition as one of the default attributes
   of the policy service.
  </para>
  
  <screen>
$ <userinput>ssoadm \
 add-attr-defs \
 --adminid amadmin \
 --password-file /tmp/pwd.txt \
 --servicename iPlanetAMPolicyConfigService \
 --schematype Organization \
 --attributevalues "iplanet-am-policy-selected-conditions=SampleCondition"</userinput>

<computeroutput>Schema attribute defaults were added.</computeroutput>
  </screen>
  
  <para>
   After completing configuration, restart OpenAM or the container in which it runs.
  </para>

  <para>
   In OpenAM Console, browse to Access Control &gt; <replaceable>Realm
   Name</replaceable> &gt; Policies &gt; <replaceable>Policy Name</replaceable>
   &gt; Conditions &gt; New.
   Notice in the list of conditions that you can now apply your Sample Condition.
  </para>
  
  <mediaobject xml:id="figure-sample-condition">
   <alt>Sample Condition in list of conditions</alt>
   <imageobject>
    <imagedata fileref="images/sample-condition.png" format="PNG"/>
   </imageobject>
   <textobject>
    <para>
     The sample condition you added is present in the list of conditions.
    </para>
   </textobject>
  </mediaobject>
 </section>

 <section xml:id="extend-ssoadm-classpath">
  <title>Extending the ssoadm Classpath</title>
  <para>
   After customizing your OpenAM deployment to use policy evaluation plugins,
   inform <command>ssoadm</command> users to add the jar file containing the
   plugins to the classpath before running policy management subcommands.
  </para>
  <para>
   To add a jar file to the <command>ssoadm</command> classpath, set the
   <literal>CLASSPATH</literal> environment variable before running the
   <command>ssoadm</command> command.
   <screen>
    $ <userinput>export CLASSPATH=<replaceable>/path/to/jarfile</replaceable>:$CLASSPATH</userinput>
    $ <userinput>ssoadm ...</userinput>
   </screen>
  </para>
 </section>
</chapter>
